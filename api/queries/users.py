from typing import List, Literal
from pydantic import BaseModel

class UserOut(BaseModel):
    id: str
    first: str
    last: str
    avatar: str
    email: str
    username: str

class UserListOut(BaseModel):
    users: list[UserOut]

class UserIn(BaseModel):
    first: str
    last: str
    avatar: str
    email: str
    username: str

# Implement the following methods so that the get all, get by id, create, 
# and delete endpoints work and can call these methods
# Start with create and get, then move on to the other two.
#
# If you want more practice, implement the following which will require
# more methods listed in the mongodb documentation:
# - endpoint + query method for - find truck by Cuisine 
# (should return all of the trucks for a given cuisine input)
# - endpoint + query method for - update truck


class UserQueries:
    def get_all_users(self) -> List[UserOut]:
        pass
    
    def get_user(self, user_id: str) -> UserOut:
        pass

    def delete_user(self, user_id: str) -> None:
        pass
    
    def create_user(self, user: UserIn) -> UserOut:
        pass